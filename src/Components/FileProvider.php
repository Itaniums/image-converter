<?php
declare(strict_types=1);

namespace App\Components;

use App\Components\ImageEditor\MimeTypeExtension;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileProvider
{
	/**
	 * @var Filesystem
	 */
	private $filesystem;
	/**
	 * @var string
	 */
	private $path;

	/**
	 * FileProvider constructor.
	 * @param Filesystem $filesystem
	 * @param string $path
	 */
	public function __construct(Filesystem $filesystem, string $path)
	{
		$this->filesystem = $filesystem;
		$this->path = $path;
	}

	/**
	 * Create file path name and extension.
	 *
	 * @param string $filename
	 * @param \SplFileObject $file
	 * @return string
	 * @throws \Exception
	 */
	public function getPath(string $filename, \SplFileObject $file): string
	{
		return $this->path . $filename . '.' . $this->getFileExtension($file);
	}

	/**
	 * Save content to filesystem
	 *
	 * @param string $path
	 * @param string $content
	 * @return File
	 * @throws \Exception
	 */
	public function save(string $path, string $content): File
	{
		try {
			$this->filesystem->dumpFile($path, $content);
		} catch (IOException $e) {
			throw new \Exception('Error: ' . $e->getMessage());
		}
		return $this->load($path);
	}

	/**
	 * Read mime-type from file header
	 *
	 * @param \SplFileObject $file
	 * @return string
	 */
	public function getMimeType(\SplFileObject $file): string
	{
		$finfo = new \finfo(FILEINFO_MIME_TYPE);
		return $finfo->file($file->getPathname());
	}

	/**
	 * Get file extension
	 *
	 * @param \SplFileObject $file
	 * @return mixed
	 * @throws \Exception
	 */
	public function getFileExtension(\SplFileObject $file): string
	{
		return MimeTypeExtension::getExtension($this->getMimeType($file));
	}

	/**
	 * Read binary content
	 *
	 * @param \SplFileObject $file
	 * @return string
	 */
	public function readContent(\SplFileObject $file): string
	{
		$buffer = '';
		while (!$file->eof()) {
			$buffer .= $file->fgets();
		}

		return $buffer;
	}

	/**
	 * Load file from filesystem
	 *
	 * @param string $path
	 * @return File
	 * @throws \Exception
	 */
	public function load(string $path): File
	{
		if (false === $this->filesystem->exists($path)) {
			throw new NotFoundHttpException("File `{$path}` not found!");
		}

		return new File($path);
	}
}
