<?php
declare(strict_types=1);

namespace App\Components\ImageEditor;

class Converter
{
	/** @var int  */
	private $maxWidth;
	/** @var int  */
	private $maxHeight;
	/** @var ImageEditorInterface */
	private $editor;

	/**
	 * Converter constructor.
	 * @param ImageEditorInterface $editor
	 * @param int $maxWidth
	 * @param int $maxHeight
	 */
	public function __construct(ImageEditorInterface $editor, int $maxWidth, int $maxHeight)
	{
		$this->editor = $editor;
		$this->maxWidth = $maxWidth;
		$this->maxHeight = $maxHeight;
	}

	/**
	 * Convert image
	 *
	 * @param string $binaryFile
	 * @return string
	 */
	public function convert(string $binaryFile) : string
	{
		return $this->editor->scale($binaryFile, $this->maxWidth, $this->maxHeight);
	}

	/**
	 * @param int $maxHeight
	 */
	public function setMaxHeight(int $maxHeight): void
	{
		$this->maxHeight = $maxHeight;
	}

	/**
	 * @param int $maxWidth
	 */
	public function setMaxWidth(int $maxWidth): void
	{
		$this->maxWidth = $maxWidth;
	}
}