<?php

namespace App\Components\ImageEditor;

interface ImageEditorInterface
{
	/**
	 * Scale image
	 *
	 * @param string $content
	 * @param int $width
	 * @param int $height
	 * @return string
	 */
	public function scale(string $content, int $width, int $height): string;
}