<?php
declare(strict_types=1);

namespace App\Components\ImageEditor;

class ImagickEditor implements ImageEditorInterface
{
	/**
	 * @var \Imagick
	 */
	private $imagick;

	/**
	 * ImagickEditor constructor.
	 * @param \Imagick $imagick
	 */
	public function __construct()
	{
		$this->imagick = new \Imagick();
	}

	/**
	 * @inheritdoc
	 */
	public function scale(string $content, int $width, int $height): string
	{
		$this->imagick->readImageBlob($content);
		try {
			$this->imagick->thumbnailImage($width, $height, true);
		} catch (\ImagickException $e) {
			$this->imagick->thumbnailImage($width, $height, false);
		}
		return $this->imagick->getImageBlob();
	}
}