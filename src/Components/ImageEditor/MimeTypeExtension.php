<?php
declare(strict_types=1);

namespace App\Components\ImageEditor;

class MimeTypeExtension
{
	CONST SUPPORTED_MIME_TYPES = [
		'image/gif' => 'gif',
		'image/jpeg' => 'jpg',
		'image/pjpeg' => 'jpg',
		'image/png' => 'png',
		'image/svg+xml' => 'svg',
		'image/tiff' => 'tiff',
	];

	/**
	 * @param string $mimeType
	 * @return mixed
	 * @throws \Exception
	 */
	public static function getExtension(string $mimeType)
	{
		if (false === array_key_exists($mimeType, self::SUPPORTED_MIME_TYPES)) {
			throw new \Exception("Mime type {$mimeType} is not supported.");
		}
		return self::SUPPORTED_MIME_TYPES[$mimeType];
	}
}
