<?php
declare(strict_types=1);

namespace App\Components;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;

class Normalizer
{
	/**
	 * @var DataUriNormalizer
	 */
	private $normalizer;

	/**
	 * Normalizer constructor.
	 * @param DataUriNormalizer $normalizer
	 */
	public function __construct(DataUriNormalizer $normalizer)
	{
		$this->normalizer = $normalizer;
	}

	/**
	 * @param $content
	 * @return array|bool|float|int|string
	 */
	public function normalize($content)
	{
		return $this->normalizer->normalize($content);
	}

	/**
	 * @param $content
	 * @return \SplFileObject
	 */
	public function denormalize($content): \SplFileObject
	{
		try {
			return $this->normalizer->denormalize($content, \SplFileObject::class);
		} catch (NotNormalizableValueException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}
	}
}