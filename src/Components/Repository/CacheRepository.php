<?php
declare(strict_types=1);

namespace App\Components\Repository;

use Symfony\Component\Cache\Simple\AbstractCache;

class CacheRepository
{
	/**
	 * @var AbstractCache
	 */
	private $cacheProvider;

	/**
	 * CacheRepository constructor.
	 * @param AbstractCache $cacheProvider
	 */
	public function __construct(AbstractCache $cacheProvider)
	{
		$this->cacheProvider = $cacheProvider;
	}

	/**
	 * Save content to cache
	 *
	 * @param string $id
	 * @param string $content
	 * @return bool
	 */
	public function save(string $id, string $content): bool
	{
		return $this->cacheProvider->set($id, $content);
	}

	/**
	 * Load content from cache
	 *
	 * @param $id
	 * @return string|null
	 */
	public function load($id)
	{
		return $this->cacheProvider->get($id);
	}
}