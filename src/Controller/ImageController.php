<?php

namespace App\Controller;

use App\Components\Repository\CacheRepository;
use App\Components\ImageEditor\Converter;
use App\Components\Normalizer;
use App\Components\FileProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
	/**
	 * @Route("/upload", name="upload", methods="PUT")
	 */
	public function upload(
		Request $request,
		Normalizer $normalizer,
		FileProvider $fileProvider,
		CacheRepository $repository,
		Converter $converter
	) {
		$splFile = $normalizer->denormalize($request->getContent());
		$id = uniqid();
		try {
			$path = $fileProvider->getPath($id, $splFile);
			$file = $converter->convert($fileProvider->readContent($splFile));
			$splFile = $fileProvider->save($path, $file);
			$repository->save($id, $splFile->getPathname());
		} catch (Exception $e) {
			throw new \LogicException('Convert and save image error: ' . $e->getMessage());
		}
		return new JsonResponse(['id' => $id], Response::HTTP_CREATED);
	}

	/**
	 * @Route("/download/{id}", name="download", methods="GET")
	 */
	public function download(
		$id,
		Request $request,
		FileProvider $fileProvider,
		CacheRepository $repository,
		Converter $converter
	) {
		if (null === $repository->load($id)) {
			throw new NotFoundHttpException('Image not found.');
		}
		$file = $fileProvider->load($repository->load($id));
		$image = $fileProvider->readContent($file->openFile());
		if (null !== $request->get('height') || null !== $request->get('width')) {
			$converter->setMaxHeight($request->get('height') ?? 0);
			$converter->setMaxWidth($request->get('width') ?? 0);
			$image = $converter->convert($image);
		}
		return new Response($image, Response::HTTP_OK, ['Content-type' => $file->getMimeType()]);
	}
}
