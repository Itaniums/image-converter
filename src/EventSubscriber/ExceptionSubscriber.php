<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionSubscriber implements EventSubscriberInterface
{
	/**
	 * {@inheritdoc}
	 */
	public static function getSubscribedEvents()
	{
		return array(
			KernelEvents::EXCEPTION => 'processException'
		);
	}

	/**
	 * @param GetResponseForExceptionEvent $event
	 */
	public function processException(GetResponseForExceptionEvent $event)
	{
		$exception = $event->getException();

		$instancClass = get_class($exception);

		switch ($instancClass) {
			case NotFoundHttpException::class:
				$response = new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
				break;
			case BadRequestHttpException::class:
				$response = new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
				break;
			default:
				$response = new JsonResponse(
					[
						'message' => $exception->getMessage(),
						'file' => $exception->getFile(),
						'line' => $exception->getLine()
					]
				);
				break;
		}
		$event->setResponse($response);
	}
}
